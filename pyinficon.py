import serial
import sys

class PressureGauge(object):
    def __init__(self,address):
        self.address = address
        self.inficon_address = 0
        self.calcCrc16()

    def calcCrc16(self):
        polynomial = 0x8408
        res = []
        for i in range(0,256):
            value = 0
            temp = i
            i += 1
            for j in range(0,8):
                if(((value ^ temp) & 0x0001) != 0):
                    value = 0xFFFF & ((value >> 1) ^ polynomial)
                else:
                    value = value >> 1
                temp = temp >> 1
            res.append(value)
        self.crc16table = res

    def createRequest(self, pid, cmdid, data, datalen):
        print(int(pid/256))
        cmd = [self.inficon_address,
               0, # Master
               0, # Ack
               11 + datalen - (3+1) -2, # msg length
               cmdid, # cmd
               int(pid/256) & 0xFF, 
               pid & 0xFF,
               0, # reserved
               0, # reserved
               ]
        cmd.append(data)
        crc = self.codeMessage(cmd, 11 + datalen - 2)
        cmd.append(int(crc / 256) & 0xFF)
        cmd.append(crc & 0xFF)
        res = self.sendRequest(bytearray(cmd))
        return res
    
    def sendRequest(self, cmd):
        with serial.Serial(self.address, 57600, timeout=0.5) as ser:
            ser.write(cmd)
            res = ser.readlines()
        return res
    
    def inficonCrc16Check(self, data, length):
        return self.codeMessage(data, length) == 0
    
    def inficonCheck(self, data, length, cmd):
        addr = self.inficon_address
        
        if length < 11:
            print("No reply from gauge\n")
            sys.exit()
        if self.inficonCrc16Check(data, length):
            print("Invalid CRC from gauge\n")
            sys.exit()
            
    def codeMessage(self, data, length):
        initial = 0xFFFF
        crc = initial
        for i in range(0,length):
            crc = 0xFFFF & ((self.crc16table[(crc ^ data[i]) & 0xFF]) ^ (crc >> 8))
            i += 1
        return crc
    
    def readPressure(self):
        pressure = self.createRequest(0x00DD, 1, 0, 0)
        print(pressure)

if __name__ == '__main__':
    gauge = PressureGauge('/dev/ttyr01')
    gauge.readPressure()
    
